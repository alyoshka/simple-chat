package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// Server describes chat Server
type Server struct {
	pool map[int]*Client
	add  chan *Client
	del  chan *Client
	send chan *message
}

// NewServer returns initialized Server instance
func NewServer() *Server {
	return &Server{
		pool: make(map[int]*Client),
		add:  make(chan *Client),
		del:  make(chan *Client),
		send: make(chan *message),
	}
}

// Add client to pool
func (s *Server) Add(client *Client) {
	s.add <- client
}

// Delete client from pool
func (s *Server) Delete(client *Client) {
	s.del <- client
}

// Send sends messages to clients
func (s *Server) Send(msg *message) {
	s.send <- msg
}

// Handle new websocket connections
func (s *Server) Handle(w http.ResponseWriter, r *http.Request) {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     func(r *http.Request) bool { return true },
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print(err)
		return
	}
	defer conn.Close()
	client := NewClient(conn, s, r.Header.Get(usernameHeader))
	s.Add(client)
	client.Listen()
}

// Listen listens and serves connections
func (s *Server) Listen() {
	log.Print("starting server")
	http.HandleFunc("/", s.Handle)

	for {
		select {
		case client := <-s.add:
			s.pool[client.ID] = client
			go s.Send(&message{
				Info: fmt.Sprintf("%s is connected", client.Username),
			})
			log.Printf("connected client id=%d", client.ID)

		case client := <-s.del:
			delete(s.pool, client.ID)
			go s.Send(&message{
				Info: fmt.Sprintf("%s is disconnected", client.Username),
			})
			log.Printf("client id=%d disconnected", client.ID)

		case m := <-s.send:
			for id, client := range s.pool {
				if id != m.ID {
					client.Write(m)
				}
			}
		}
	}
}
