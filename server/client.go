package main

import (
	"math/rand"
	"time"

	"github.com/gorilla/websocket"
)

// Client describes client connection
type Client struct {
	ID       int
	Username string
	conn     *websocket.Conn
	server   *Server
	close    chan bool
	send     chan *message
}

// NewClient initializes new Client instance
func NewClient(conn *websocket.Conn, server *Server, username string) *Client {
	rand.Seed(time.Now().UTC().UnixNano())
	return &Client{
		ID:       rand.Int(),
		Username: username,
		conn:     conn,
		server:   server,
		close:    make(chan bool),
		send:     make(chan *message),
	}
}

// Listen listens for a new messages
func (c *Client) Listen() {
	go c.write()
	c.read()
}

// Write sends message to client
func (c *Client) Write(m *message) {
	c.send <- m
}

func (c *Client) read() {
	for {
		select {
		case <-c.close:
			c.server.Delete(c)
			c.close <- true
			return

		default:
			_, msg, err := c.conn.ReadMessage()
			if err != nil {
				c.server.Delete(c)
				c.close <- true
				return
			}
			c.server.Send(&message{
				ID:      c.ID,
				From:    c.Username,
				Payload: string(msg),
			})
		}
	}
}

func (c *Client) write() {
	for {
		select {
		case <-c.close:
			c.server.Delete(c)
			c.close <- true
			return

		case msg := <-c.send:
			err := c.conn.WriteJSON(msg)
			if err != nil {
				c.server.Delete(c)
				c.close <- true
				return
			}
		}
	}
}
