package main

// usernameHeader is header where clients store username
const usernameHeader = "Username"

// message describes message in chat
type message struct {
	ID      int
	From    string `json:"from,omitempty"`
	Payload string `json:"payload,omitempty"`
	Info    string `json:"info,omitempty"`
}
