# Simple chat server

Simple chat server with websockets

## How to use
Install gorilla websockets lib
+ go get github.com/gorilla/websocket

Build server
+ go build

Start server
+ ./server [--port=PORT_TO_LISTEN]
