package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	port := flag.Int("port", 8080, "Port to listen")
	flag.Parse()

	s := NewServer()
	go s.Listen()
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}
