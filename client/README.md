# Simple chat client

Simple chat client with websockets

## How to use
Install gorilla websockets lib
+ go get github.com/gorilla/websocket

Build client
+ go build

Start client
+ ./client [--url=SERVER_URL] [--username=USERNAME]
