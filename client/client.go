package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/websocket"
)

// usernameHeader is header where clients store username
const usernameHeader = "Username"

// message describes message in chat
type message struct {
	From    string `json:"from,omitempty"`
	Payload string `json:"payload,omitempty"`
	Info    string `json:"info,omitempty"`
}

// Client describes chat client
type Client struct {
	conn  *websocket.Conn
	close bool
}

// NewClient initializes new client instance
func NewClient(url string, username string) (*Client, error) {
	dialer := &websocket.Dialer{}
	header := http.Header{}
	header.Add(usernameHeader, username)
	conn, _, err := dialer.Dial(fmt.Sprintf("ws://%s/", url), header)
	if err != nil {
		return nil, err
	}
	return &Client{
		conn: conn,
	}, nil
}

func (c *Client) listen() {
	for {
		msg, err := c.read()
		if err != nil {
			c.close = true
			fmt.Print("Connection closed by server. Press any key to exit")
			return
		}
		fmt.Print(msg)
	}
}

// Start client
func (c *Client) Start() {
	go c.listen()

	reader := bufio.NewReader(os.Stdin)
	for !c.close {
		text, err := reader.ReadString('\n')
		if err != nil {
			c.close = true
		}
		c.Send(text)
	}
}

// Send message to chat
func (c *Client) Send(msg string) error {
	err := c.conn.WriteMessage(websocket.TextMessage, []byte(msg))
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) read() (string, error) {
	var msg message
	err := c.conn.ReadJSON(&msg)
	if err != nil {
		return "", err
	}
	if msg.Info != "" {
		return fmt.Sprintln(msg.Info), nil
	}
	return fmt.Sprintf("[%s]: %s", msg.From, msg.Payload), nil
}

func main() {
	username := flag.String("username", "John Doe", "Username")
	url := flag.String("url", "localhost:8080", "Server URL")
	flag.Parse()

	c, err := NewClient(*url, *username)
	if err != nil {
		log.Printf("failed to connect: %s", err)
		return
	}
	c.Start()
}
